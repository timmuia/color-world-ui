import BoxIconElement from "boxicons/src/box-icon-element";
import {FaRegStar, FaUpload} from "react-icons/fa";
import {useRef, useState} from "react";
import axiosClient from "../../axios-client";
import {Navigate} from "react-router-dom";
import FeebbackNotification from "./FeebbackNotification";

const FeedbackForm = () => {

    const nameRef = useRef();
    const emailRef = useRef();
    const phoneRef = useRef();
    const customerCategoryRef = useRef();

    const [errors, setErrors] = useState(null);

    const stars = Array(5).fill(0);
    const [productExperienceRating, setProductExperienceRating] = useState(0);
    const [customerCareRating, setCustomerCareRating] = useState(0);
    const [productSatisfactionRating, setProductSatisfactionRating] = useState(0);
    const [productExperienceHoverValue, setProductExperienceHoverValue] = useState(undefined);
    const [customerCareHoverValue, setCustomerCareHoverValue] = useState(undefined);
    const [productSatisfactionHoverValue, setProductSatisfactionHoverValue] = useState(undefined);
    const [redirect, setRedirect] = useState();

    const handleProductExperienceRatingClick = value => {
        setProductExperienceRating(value);
    }

    const handleProductExperienceRatingHoverIn = value => {
        setProductExperienceHoverValue(value);
    }

    const handleProductExperienceRatingHoverOut = value => {
        setProductExperienceHoverValue(undefined);
    }

    const handleCustomerCareRatingClick = value => {
        setCustomerCareRating(value);
    }

    const handleCustomerCareHoverIn = value => {
        setCustomerCareHoverValue(value);
    }

    const handleCustomerCareHoverOut = value => {
        setCustomerCareHoverValue(undefined);
    }

    const handleProductSatisfactionRatingClick = value => {
        setProductSatisfactionRating(value);
    }

    const handleProductSatisfactionRatingHoverIn = value => {
        setProductSatisfactionHoverValue(value);
    }

    const handleProductSatisfactionRatingHoverOut = () => {
        setProductSatisfactionHoverValue(undefined);
    }

    const onSubmit = (ev) =>{
        ev.preventDefault()

        const payload = {
            name: nameRef.current.value,
            email: emailRef.current.value,
            phone: phoneRef.current.value,
            customer_category_id: customerCategoryRef.current.value,
            product_experience: productExperienceRating,
            customer_care: customerCareRating,
            product_satisfaction: productSatisfactionRating
        }

        axiosClient.post('/feedbacks', payload)
            .then(response => {
                setRedirect('/feedbacks/success')
            })
            .catch(error => {
                const response = error.response;

                if (response && response.status === 422) {
                    if (response.data.errors) {
                        setErrors(response.data.errors);
                    } else {
                        setErrors({
                            email: [response.data.message]
                        });
                    }
                }
            });

    }

    if(redirect){
        return <Navigate to="/feedbacks/success" />
    }

    return (
        <div className="bg-white rounded-lg shadow-lg">
            <div className="bg-blue-100 p-12 py-6 rounded-t-lg">
                <div className="text-blue-900 text-4xl tracking-wider">
                    CUSTOMER FEEDBACK
                </div>
                <div className="text-gray-600">
                    Send us your feedback to help us improve on our service to all our customers
                </div>
            </div>
            <div className="px-12 py-6">
                <form onSubmit={onSubmit} className="space-y-6">
                    {errors && <div className="bg-red-300 p-2 rounded-lg my-6">
                        <div className="p-2 bg-red-300 border-2 border-red-700 rounded-full w-fit">
                            <svg className="fill-current text-red-700 w-4 h-4 mr-" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
                        </div>
                        {Object.keys(errors).map(key => (
                            <p className="text-red-700 text-sm" key={key}> {errors[key][0]}</p>
                        ))}
                    </div> }
                    <div className="flex flex-col">
                        <div className="text-gray-600">
                            Name: <span className="text-red-700">*</span>
                        </div>
                        <div className="mt-4">
                            <input ref={nameRef} className="border-b-2 border-blue-200 focus:border-b-2 focus:border-blue-700 focus:outline-none w-full"/>
                        </div>
                    </div>
                    <div className="flex flex-col">
                        <div className="text-gray-600">
                            Email: <span className="text-red-700">*</span>
                        </div>
                        <div className="mt-4">
                            <input ref={emailRef} className="border-b-2 border-blue-200 focus:border-b-2 focus:border-blue-700 focus:outline-none w-full"/>
                        </div>
                    </div>
                    <div className="flex flex-col">
                        <div className="text-gray-600">
                            Phone Number: <span className="text-red-700">*</span>
                        </div>
                        <div className="mt-4">
                            <input ref={phoneRef} className="border-b-2 border-blue-200 focus:border-b-2 focus:border-blue-700 focus:outline-none w-full"/>
                        </div>
                    </div>
                    <div className="flex flex-col">
                        <div className="text-gray-600">
                            Customer Category: <span className="text-red-700">*</span>
                        </div>
                        <div className="mt-4">
                            <select ref={customerCategoryRef} className="w-full p-2 border-2 border-blue-200 text-gray-600 bg-white rounded focus:border-blue-700 focus:outline-none">
                                <option value="">Please select a category</option>
                                <option value="1">Paint Buyer</option>
                                <option value="2">Painter</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <div className="text-gray-600">
                            Rate your experience with our product?
                        </div>
                        <div className="mt-4 flex flex-row space-x-6">
                            {stars.map((_, index) => {
                                return (
                                    <FaRegStar
                                        onClick={() => handleProductExperienceRatingClick(index + 1)}
                                        onMouseOver={() => handleProductExperienceRatingHoverIn(index + 1) }
                                        onMouseOut={() => handleProductExperienceRatingHoverOut()}
                                        key={index}
                                        className={`text-3xl ${(productExperienceHoverValue || productExperienceRating) > index ? "text-amber-500" : "text-gray-400"}`} />
                                )
                            })}
                        </div>
                    </div>
                    <div>
                        <div className="text-gray-600">
                            Rate our customer care?
                        </div>
                        <div className="mt-4 flex flex-row space-x-6">
                            {stars.map((_, index) => {
                                return (
                                    <FaRegStar
                                        onClick={() => handleCustomerCareRatingClick(index + 1)}
                                        onMouseOver={() => handleCustomerCareHoverIn(index + 1) }
                                        onMouseOut={() => handleCustomerCareHoverOut()}
                                        key={index}
                                        className={`text-3xl ${(customerCareHoverValue || customerCareRating) > index ? "text-amber-500" : "text-gray-400"}`} />
                                )
                            })}
                        </div>
                    </div>
                    <div>
                        <div className="text-gray-600">
                            Are you happy with the product?
                        </div>
                        <div className="mt-4 flex flex-row space-x-6">
                            {stars.map((_, index) => {
                                return (
                                    <FaRegStar
                                        onClick={() => handleProductSatisfactionRatingClick(index + 1)}
                                        onMouseOver={() => handleProductSatisfactionRatingHoverIn(index + 1) }
                                        onMouseOut={() => handleProductSatisfactionRatingHoverOut()}
                                        key={index}
                                        className={`text-3xl ${(productSatisfactionHoverValue || productSatisfactionRating) > index ? "text-amber-500" : "text-gray-400"}`} />
                                )
                            })}
                        </div>
                    </div>

                    <div>
                        <button className="bg-blue-900 flex flex-row space-x-2 py-2 px-6 rounded text-white">
                            <FaUpload className="self-center" /> &nbsp; SUBMIT
                        </button>
                    </div>
                </form>
            </div>
        </div>

    )
}

export default FeedbackForm;