import {FaUsers} from "react-icons/fa";
import {FaArrowTrendUp} from "react-icons/fa6";
import {CategoryScale, Chart, Filler, LinearScale, LineElement, PointElement} from "chart.js";
import { Line} from "react-chartjs-2";

Chart.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Filler
)

export default function Doughnut(){

    const data = {
        labels: ["red","blue","green"],
        datasets: [{
            data: [2, 3, 4],
            tension: 0.5,
            backgroundColor: ["#0047AB40"],
            borderColor: "#0047AB80",
            borderWidth: 1,
            fill: true
        }]
    };

    const options = {
        plugins: {
            legend: false
        },
        scales: {
            x: {
                grid: {
                    display: false
                },
            },
            y: {
                ticks: {
                    stepSize: 2
                }
            }
        }
    }

    return(
        <div>
            <div className="mt-4 flex flex-col md:flex-row space-x-3">
                <div className="w-full md:w-2/3 p-2 rounded-lg bg-white">
                    <div className="flex justify-between">
                        <div className="flex flex-row items-center space-x-2 text-sm">
                            <div>
                                This week's trend
                            </div>
                            <div className="bg-blue-200 p-2 rounded-full">
                                <FaArrowTrendUp className="text-blue-900"/>
                            </div>
                        </div>
                    </div>
                    <div>
                        <Doughnut className="bg-gray-50" data={data} options={options}></Doughnut>
                    </div>
                </div>
            </div>
        </div>
    )
}