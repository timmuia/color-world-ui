import {Link} from "react-router-dom";
import {FaCheck} from "react-icons/fa";

const FeebbackNotification = () => {

    return(
        <div className="bg-white rounded-lg p-12 shadow-lg">
            <div className="flex justify-center">
                <div className="p-4 bg-gray-100 rounded-full border-4 border-green-600">
                    <FaCheck className="text-4xl text-green-600"></FaCheck>
                </div>
            </div>
            <div className="mt-8">
                Feedback has been submitted successfully!
            </div>
            <div className="flex justify-center mt-4 ">
                <Link to={'/new-feedback'} className="text-blue-900">Submit Another</Link>
            </div>
        </div>
    )
}

export default FeebbackNotification;