import {useEffect, useState} from "react";
import axiosClient from "../../axios-client";
import {Link} from "react-router-dom";

const Users = () => {

    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getUsers()
    }, []);

    const getUsers = () => {
        setLoading(true)
        axiosClient.get('/users')
            .then(({data}) => {
                console.log(data)
                setUsers(data.data)
                setLoading(false)
            }).catch(()=>{
                setLoading(false)
        })
    }

    return (
        <div className="bg-white p-2 rounded-lg">
            <div className="p-6">
                <div className="text-lg">
                    Users
                </div>
            </div>
            <hr/>
            <div className="p-2 mt-4">
                <div>
                    <table className="w-full table table-auto shadow rounded-lg">
                        <thead className="rounded-lg">
                        <tr className="space-y-3 bg-blue-100 p-2 rounded-lg">
                            <th className="text-start p-4">ID</th>
                            <th className="text-start p-4">Name</th>
                            <th className="text-start p-4">Email</th>
                            <th className="text-start p-4">Phone</th>
                            <th className="text-start p-4">Customer Category</th>
                            <th className="text-start p-4">Date Created</th>
                            <th className="text-start p-4">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {users.map(user => (
                            <tr className="p-4 border-b-2 border-gray-300 rounded hover:bg-gray-100">
                                <td className="p-4 border-b-2 border-gray-300">{user.id}</td>
                                <td><div className="text-blue-900">{user.name}</div></td>
                                <td>{user.email}</td>
                                <td>{user.phone}</td>
                                <td>{(user.customer_category_id == 1) ? 'Paint Buyer' : 'Painter'}</td>
                                <td>{user.created_at}</td>
                                <td><Link to={'/users/' + user.id}>View</Link></td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    )
}

export default Users;