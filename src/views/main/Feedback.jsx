import {useParams} from "react-router-dom";
import axiosClient from "../../axios-client";
import {useEffect, useState} from "react";
import {FaRegStar, FaUser} from "react-icons/fa";

const Feedback = () => {

    const {id} = useParams()
    const [feedback, setFeedback] = useState({})
    const stars = Array(5).fill(0);

    useEffect(() => {
        fetchFeedback()
    }, []);

    const fetchFeedback = () => {

        axiosClient.get('/feedbacks/' + id)
            .then(({data}) => {
                setFeedback(data);
            })
    }

    return (
        <div className="bg-white rounded-lg">
            <div>
                <div className="p-6 pb-2">
                    <div className="text-lg">
                        Feedback Info
                    </div>
                </div>
                <hr/>
                <div className="px-6 py-4">

                    <div className="border-2 border-gray-200 w-full rounded-lg">
                        <div className="p-2">
                            Customer Info
                        </div>
                        <div className="flex flex-row">
                            <div className="p-2 flex justify-center items-center">
                                <div className="bg-blue-200 p-4 rounded-full">
                                    <FaUser className="text-white text-7xl" />
                                </div>
                            </div>
                            <div className="flex flex-col ">
                                <div className="flex flex-row space-x-2 p-2">
                                    <span className="text-gray-700">Name</span>: <span className="text-blue-900">{feedback.name}</span>
                                </div>
                                <div className="flex flex-row space-x-2 p-2">
                                    <span className="text-gray-700">Phone</span>: <span className="text-blue-900">{feedback.phone}</span>
                                </div>
                                <div className="flex flex-row space-x-2 p-2">
                                    <span className="text-gray-700">Email</span>: <span className="text-blue-900">{feedback.email}</span>
                                </div>
                                <div className="flex flex-row space-x-2 p-2">
                                    <span className="text-gray-700">Customer Category</span>: <span className="text-blue-900">{(feedback.customer_category_id == 1) ? 'Paint Buyer' : 'Painter'}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mt-10">
                        <div className="text-gray-600">
                            Rate your experience with our product?
                        </div>
                        <div className="mt-4 flex flex-row space-x-6">
                            {stars.map((_, index) => {
                                return (
                                    <FaRegStar key={index}
                                        className={`text-3xl ${(feedback.product_experience) > index ? "text-amber-500" : "text-gray-400"}`} />
                                )
                            })}
                        </div>
                    </div>
                    <div className="mt-10">
                        <div className="text-gray-600">
                            Rate our customer care?
                        </div>
                        <div className="mt-4 flex flex-row space-x-6">
                            {stars.map((_, index) => {
                                return (
                                    <FaRegStar key={index}
                                               className={`text-3xl ${(feedback.customer_care) > index ? "text-amber-500" : "text-gray-400"}`} />
                                )
                            })}
                        </div>
                    </div>
                    <div className="mt-10">
                        <div className="text-gray-600">
                            Are you happy with the product?
                        </div>
                        <div className="mt-4 flex flex-row space-x-6">
                            {stars.map((_, index) => {
                                return (
                                    <FaRegStar key={index}
                                               className={`text-3xl ${(feedback.product_satisfaction) > index ? "text-amber-500" : "text-gray-400"}`} />
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
            <div>

            </div>
        </div>
    )
}

export default Feedback