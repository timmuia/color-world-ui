import {FaUsers, FaUsersCog} from "react-icons/fa";
import {FaArrowTrendUp, FaUsersRays} from "react-icons/fa6";
import axiosClient from "../../axios-client";
import {useEffect, useState} from "react";
import Chart from "react-apexcharts";

export default function Dashboard(){

    const [customersCount, setCustomersCount] = useState({});
    const [customersFeedback, setCustomersFeedback] = useState([]);

    useEffect(() => {
        fetchCustomersCount()
    }, []);


    const fetchCustomersCount = () => {

        axiosClient.get('/dashboard/customers')
            .then(({data}) => {
                setCustomersCount(data)
            })

        axiosClient.get('/dashboard/customers-feedbacks')
            .then(({data}) => {
                setCustomersFeedback(data)
            })
    }

    const state = {
        options: {
            labels: customersFeedback && customersFeedback.donut  && customersFeedback.donut.labels ? customersFeedback.donut.labels : []
        },
        series: customersFeedback && customersFeedback.donut  && customersFeedback.donut.series ? customersFeedback.donut.series : [],
    }

    const line = {
        options: {
            chart: {
                id: "basic-bar"
            },
            xaxis: {
                categories: customersFeedback && customersFeedback.line  && customersFeedback.line.labels ? customersFeedback.line.labels : []
            }
        },
        series: [
            {
                name: "Buyers",
                data: customersFeedback && customersFeedback.line  && customersFeedback.line.series && customersFeedback.line.series.buyers ? customersFeedback.line.series.buyers : []
            },
            {
                name: "Painters",
                data: customersFeedback && customersFeedback.line  && customersFeedback.line.series && customersFeedback.line.series.painters ? customersFeedback.line.series.painters : []
            }
        ]
    };

    return(
        <div>
            <div className="p-2">
                <div className="flex flex-row space-x-6">
                    <div className=" py-6 px-16 shadow rounded-lg bg-blue-100 w-fit flex flex-col">
                        <div className="flex justify-center">
                            <FaUsers className="text-5xl text-blue-400"/>
                        </div>
                        <label className="text-blue-900 font-bold">Total participants</label>
                        <div className="flex justify-center text-blue-900">
                            {customersCount.all}
                        </div>
                    </div>
                    <div className=" py-6 px-16 shadow rounded-lg bg-green-100 w-fit flex flex-col">
                        <div className="flex justify-center">
                            <FaUsersRays className="text-5xl text-green-400"/>
                        </div>
                        <label className="text-green-900 font-bold">Paint Buyers</label>
                        <div className="flex justify-center text-green-900">
                            {customersCount.buyers}
                        </div>
                    </div>
                    <div className=" py-6 px-16 shadow rounded-lg bg-red-100 w-fit flex flex-col">
                        <div className="flex justify-center">
                            <FaUsersCog className="text-5xl text-red-400"/>
                        </div>
                        <label className="text-red-900 font-bold">Painters</label>
                        <div className="flex justify-center text-red-900">
                            {customersCount.painters}
                        </div>
                    </div>
                </div>
            </div>
            <div className="mt-4 flex flex-col md:flex-row space-x-3">
                <div className="w-full md:w-2/3 p-2 rounded-lg bg-white">
                    <div className="flex justify-between">
                        <div className="flex flex-row items-center space-x-1 text-sm p-2">
                            <div className="p-2 text-gray-700 font-bold">
                                This week's trend
                            </div>
                            <div className="bg-bue-200 p2 rounded-full">
                                <FaArrowTrendUp className="text-blue-600 font-bold text-xl"/>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="mt-4 rounded-lg">
                        <Chart
                            options={line.options}
                            series={line.series}
                            type="area"
                        />
                    </div>
                </div>
                <div className="w-full md:w-1/3 p-2 rounded-lg bg-white">
                    <div className="w-full">
                        <Chart
                            options={state.options}
                            series={state.series}
                            type="donut"
                        />

                    </div>
                </div>
            </div>
        </div>
    )
}