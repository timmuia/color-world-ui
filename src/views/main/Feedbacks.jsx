import {useEffect, useState} from "react";
import axiosClient from "../../axios-client";
import {Link} from "react-router-dom";

const Feedbacks = () => {

    const [feedbacks, setFeedbacks] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetchFeedbacks()
    }, []);

    const fetchFeedbacks = () => {

        setLoading(true);
        axiosClient.get('/feedbacks')
            .then(({data}) => {
                setFeedbacks(data.data);
                setLoading(false);
            })
            .catch(() => {
                setLoading(false);
            })
    }

    return (
        <div className="bg-white p-2 rounded-lg">
            <div className="p-6">
                <div className="text-lg">
                    Feedbacks
                </div>
            </div>
            <hr/>
            <div className="p-2 mt-4">
                <div>
                    <table className="w-full table table-auto shadow rounded-lg">
                        <thead className="rounded-lg">
                        <tr className="space-y-3 bg-blue-100 p-2 rounded-lg">
                            <th className="text-start p-4">ID</th>
                            <th className="text-start p-4">Name</th>
                            <th className="text-start p-4">Product Experience</th>
                            <th className="text-start p-4">Customer Care</th>
                            <th className="text-start p-4">Product Satisfaction</th>
                            <th className="text-start p-4">Mean</th>
                            <th className="text-start p-4">Date Created</th>
                            <th className="text-start p-4">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {feedbacks.map(user => (
                            <tr key={user.id} className="p-4 border-b-2 border-gray-300 rounded hover:bg-gray-100">
                                <td className="p-4 border-b-2 border-gray-300">{user.id}</td>
                                <td><div className="text-blue-900">{user.name}</div></td>
                                <td>{user.product_experience}</td>
                                <td>{user.customer_care}</td>
                                <td>{user.product_satisfaction}</td>
                                <td className={(user.mean) >= 3 ? 'text-green-600' : 'text-red-600'}>{user.mean}</td>
                                <td>{user.created_at}</td>
                                <td><Link to={'/feedbacks/' + user.id}>View</Link></td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    )

}


export default Feedbacks;