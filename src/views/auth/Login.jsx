import {useRef, useState} from "react";
import axiosClient from "../../axios-client";
import {useStateContext} from "../../contexts/ContextProvider";

const Login = () => {

    const usernameRef = useRef();
    const passwordRef = useRef();

    const [errors, setErrors] = useState(null);
    const  {setUser, setToken} = useStateContext();

    const onSubmit  = (ev) => {
        ev.preventDefault()

        const payload = {
            email: usernameRef.current.value,
            password: passwordRef.current.value
        }

        axiosClient.post('/login', payload)
            .then(response => {
                setUser(response.data.user);
                setToken(response.data.token);
            })
            .catch(error => {
                const response = error.response;

                if (response && response.status === 422) {
                    if (response.data.errors) {
                        setErrors(response.data.errors);
                    } else {
                        setErrors({
                            email: [response.data.message]
                        });
                    }
                }
            });
    }

    return (
        <div className="p-12 bg-white rounded-lg pt-24 shadow">

            <div className="flex justify-center text-blue-900 text-3xl tracking-widest font-bold mx-20">
                Color World
            </div>

            <div className="flex justify-center mt-8">
                <h1 className="text-gray-700 font-bold">Hi, Welcome back! <i className="bxl bxl-user"></i></h1>
            </div>
            <form onSubmit={onSubmit} className="mt-10">
                {errors && <div className="bg-red-300 p-2 rounded-lg">
                    <div className="p-2 bg-red-300 border-2 border-red-700 rounded-full w-fit">
                        <svg className="fill-current text-red-700 w-4 h-4 mr-" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
                    </div>
                    {Object.keys(errors).map(key => (
                        <p className="text-red-700 text-sm" key={key}> {errors[key][0]}</p>
                    ))}
                </div> }
                <div className="mt-4">
                    <div className="relative group z-0">
                        <input ref={usernameRef} className="border-2-gray-100 border-2 appearance-none w-full py-2 px-4 text-gray-700 leading-tight transform bg-transparent
                                rounded-lg bg-gray-200 focus:duration-300 focus:outline-none focus:bg-white focus:border-blue-600 peer"
                               type="text" placeholder=" " required/>
                            <label className="text-sm absolute left-2 top-3 text-gray-400 duration-500
                               scale-75 -translate-y-5 z-10 peer-focus:bg-white peer-focus:left-0
                                 peer-focus:px-2 peer-focus:uppercase"><i className="bxl bxl-user"></i>Username</label>
                    </div>
                </div>
                <div className="mt-10">
                    <div className="relative group z-0">
                        <input ref={passwordRef} className="border-2-gray-100 border-2 appearance-none w-full py-2 px-4 text-gray-700 leading-tight transform bg-transparent
                                rounded-lg bg-gray-200yy focus:duration-300 focus:outline-none focus:bg-white focus:border-blue-600 peer"
                               id="inline-password" type="password" placeholder=" " required/>
                            <label className="text-sm absolute left-2 top-3 text-gray-400 duration-500
                               scale-75 -translate-y-5 z-10 peer-focus:bg-white peer-focus:left-0
                                 peer-focus:px-2 peer-focus:uppercase"><i className="bxl bxl-user"></i>Password</label>
                    </div>
                </div>
                <div className="mt-10 flex justify-center">
                    <button className="bg-blue-900 text-white rounded-lg px-12 py-2 text-sm">LOGIN</button>
                </div>
            </form>

            <hr className="mt-10"/>

            <div className="flex flex-col mt-8 text-sm text-gray-500">
                <div className="flex justify-center">
                    Don't have an account?
                </div>
                <div className="flex justify-center text-blue-900">
                    Please contact Color World Admin to get an account
                </div>

            </div>
        </div>
    )
}

export default Login;