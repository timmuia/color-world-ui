import { Navigate, createBrowserRouter } from "react-router-dom";
import Login from "./views/auth/Login";
import Users from "./views/main/Users";
import NotFound from "./views/NotFound";
import GuestLayout from "./components/layouts/GuestLayout";
import MainLayout from "./components/layouts/MainLayout";
import Dashboard from "./views/main/Dashboard";
import Feedbacks from "./views/main/Feedbacks";
import UserForm from "./views/main/UserForm";
import FeedbackLayout from "./components/layouts/FeedbackLayout";
import FeedbackForm from "./views/main/FeedbackForm";
import Feedback from "./views/main/Feedback";
import FeebbackNotification from "./views/main/FeebbackNotification";

const router = createBrowserRouter([
    {
        path: '/',
        element: <MainLayout />,
        children: [
            {
                path: '/',
                element: <Navigate to="/dashboard" />
            },
            {
                path:'/dashboard',
                element: <Dashboard />
            },
            {
                path: '/feedbacks',
                element: <Feedbacks />
            },
            {
                path: '/feedbacks/:id',
                element: <Feedback />
            },
            {
                path: '/users',
                element: <Users />
            },
            {
                path: '/users/new',
                element: <UserForm key="userCreate" />
            },
            {
                path: '/users/:id',
                element: <Users key="userUpdate" />
            }
        ]
    },
    {
        path: '/',
        element: <GuestLayout />,
        children: [
            {
                path: '/login',
                element: <Login />
            }
        ]
    },
    {
        path: '/',
        element: <FeedbackLayout />,
        children: [
            {
                path: '/new-feedback',
                element: <FeedbackForm />
            },
            {
                path: '/feedbacks/success',
                element:  <FeebbackNotification />
            },
        ]
    },

    {
        path: '*',
        element: <NotFound />
    }
])

export default router;