import { Link, Navigate, Outlet } from "react-router-dom";
import { useStateContext } from "../../contexts/ContextProvider";
import Sidebar from "../common/Sidebar";
import Header from "../common/Header";
import {useEffect} from "react";
import axiosClient from "../../axios-client";

const MainLayout = () => {

    const {user,token, setUser} = useStateContext()

    if(!token){
        return <Navigate to="/login" />
    }

    return (
        <div className="flex flex-col h-screen">
            <Header></Header>
            <div className="content flex flex-row flex-grow">
                <Sidebar></Sidebar>
                <div className="rounded-lg bg-gray-100 flex-grow p-3">
                    <main>
                        <Outlet ></Outlet>
                    </main>
                </div>
            </div>
        </div>
    )
}

export default MainLayout;