import { Navigate, Outlet } from "react-router-dom";
import { useStateContext } from "../../contexts/ContextProvider";

const GuestLayout = () => {

    const {token} = useStateContext()

    if(token){
        return <Navigate to="/" />
    }

    return (
        <div className="h-screen bg-gradient-to-b from-gray-100 to-gray-50 flex justify-center items-center">
            <Outlet />
        </div>
    )
}

export default GuestLayout;