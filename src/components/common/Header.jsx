import {useStateContext} from "../../contexts/ContextProvider";
import axiosClient from "../../axios-client";
import {useEffect} from "react";
import {FaArrowCircleLeft, FaDatabase, FaLinode, FaList, FaTable, FaTasks, FaUser} from "react-icons/fa";
import {FaLeftLong, FaRightFromBracket} from "react-icons/fa6";

const Header = () => {

    const {user, setUser, setToken} = useStateContext();

    const onLogout = (ev) => {
        ev.preventDefault()

        axiosClient.post('/logout', {}, {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: "Bearer " + localStorage.getItem("ACCESS_TOKEN")
        })
            .then(() => {

                setUser({})
                setToken(null)
                localStorage.removeItem('ACCESS_TOKEN')
            })
    }

    useEffect(() => {
        axiosClient.get('/user')
            .then(({data}) => {
                setUser(data)
            })
    }, []);

    return (
        <div className="bg-white flex justify-between p-6">
            <div className="text-blue-900 text-xl flex flex-row">
                <FaLinode className="mt-1" /> &nbsp; COLOR WORLD
                <div className="">
                    <input className="ml-8 px-2 py-1 rounded-lg border-2 border-blue-200 focus:outline-none text-sm" placeholder="Search..."/>
                </div>
            </div>
            <div className="flex flex-row space-x-1 p-2 rounded-full px-4 bg-blue-200">
                <div className="flex flex-row bg-gray-200 rounded-full p-2">
                    <FaRightFromBracket className="text-blue-900"/>

                </div>
                <div className="flex justify-center items-center">
                    <a href="#" onClick={onLogout} className="bg-blue-200 rounded-lg text-blue-900">Logout</a>
                </div>
            </div>
        </div>
    )
}

export default Header;