import {Link} from "react-router-dom";
import {useState} from "react";
import {FaTable, FaTasks, FaUsers} from "react-icons/fa";
import {FaTableCells} from "react-icons/fa6";

const Sidebar = () => {

    const [active, setActive] = useState('dashboard');

    const _setActive = key => {
        setActive(key)
    }

    return (
        <div className="p-2 bg-white w-52">
            <div className="flex flex-col">
                <Link key="dashboard" onClick={() => _setActive('dashboard')}
                      className={`rounded px-4 p-3 flex flex-row items-center space-x-2 ${(active == "dashboard") ? "bg-blue-100 text-blue-900" : "text-gray-600"} `}
                      to="/dashboard"><FaTableCells /> <span>Dashboard</span></Link>
                <Link key="feedbacks" onClick={() => _setActive('feedbacks')}
                      className={`rounded px-4 p-3 flex flex-row items-center space-x-2 ${(active == "feedbacks") ? "bg-blue-100 text-blue-900" : "text-gray-600"} `}
                      to="/feedbacks"><FaTasks /> <span>Feedbacks</span></Link>
                <Link key="users" onClick={() => _setActive('users')}
                      className={`rounded px-4 p-3 flex flex-row items-center space-x-2 ${(active == "users") ? "bg-blue-100 text-blue-900" : "text-gray-600"} `}
                      to="/users"><FaUsers /> <span>Users</span></Link>
            </div>
        </div>
    )
}

export default Sidebar;